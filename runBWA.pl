#!/usr/bin/env perl
#perl script to process a directory worth of .fastq files with bwa
#first argument is directory of fastq files
#second argument is te BWA index to use
#third argument is the length of the barcode
#modified for Casava 1.8+.  Here the quality scores are phred+33 so the -I option should not be used
#v1.8c modified to pipe the sam file from bwa samse directly to samtools view
#v1.8b modified to pipe the bam file from samtools view directly to samtools sort.
# perl runBWA.pl --fq tutorial --index TAIR10_chr.fasta --out tutorial 

use warnings;
use strict;
use File::Basename;
use Getopt::Long;
use Cwd;

my ($fq, $bwa_index, $optionI, $tag, $verbose, $help, );
my $bar_len = 0;
my $threads = 2;
my $out_dir = getcwd;
#print "output dir $out_dir \n";

my $options = GetOptions (
	"fq=s"		=>	\$fq,	
	"index=s"	=>	\$bwa_index,	
	"length=i"	=>	\$bar_len,	
	"optionI"	=>	\$optionI,
	"threads=i"	=>	\$threads,
	"tag=s"		=>	\$tag,	
	"out=s"		=>	\$out_dir,	
	"verbose"	=>	\$verbose,	
	"help"		=>	\$help,	
);

my $usage = "
	USAGE: runBWA.pl
		--fq		</path/to/*.fq directory>
		--index		</path/to/bwa_index/>
		--length	<barcode length> (default=0)
		--tag		<optional tag for output file>
		--optionI	**reads are illumina 1.3 - 1.7 format.  Use BWA option -I**
		--threads	<number of threads for BWA aln> (default=2)
		--out		</path/to/output/directory/> (default= ./ )
		--verbose	**verbose output**
		--help		**print this help**

Function Summary:
runBWA.pl processes fastq files with bwa aln and bwa samse and runs samtools view to convert to bam.
Samtools sort, samtools index and samtools idxstats are then used to get counts per gene.

";

die $usage if $help;
die $usage unless defined ($fq && $bwa_index);

if (defined $optionI) {
	$optionI = "-I"};

my $params = "aln -l 20 -t $threads -B $bar_len $optionI $bwa_index";

my @files = `ls $fq`;
chomp @files;

foreach my $file_in (@files) {
	$file_in = $fq."/".$file_in;
	chomp $file_in; 
	my ($filename, $path, $suffix) = fileparse($file_in, ".fq|.fastq");
	next unless length $suffix > 0;

	my $file_out_root;
	if (defined $out_dir) {
		$file_out_root = $out_dir . "/". $filename . $tag;
	}else{
		$file_out_root = $path . $filename . $tag;
	}
	
	print "starting aln of $file_in " . localtime() . " \n" if $verbose;
	print "executing bwa $params $file_in > $file_out_root.sai\n" if $verbose;
	system "bwa $params $file_in > $file_out_root.sai";
	print "starting samse of $file_in " . localtime() . " \n" if $verbose;
	print "executing bwa samse $bwa_index $file_out_root.sai $file_in | samtools view -bSh - > $file_out_root.bam";
	# | samtools view -bS - | samtools sort -m 300000000 - $file_out_root\n" if $verbose;
	system "bwa samse $bwa_index $file_out_root.sai $file_in | samtools view -bSh - > $file_out_root.bam"; 
        #samtools sort -m 300000000 - $file_out_root";
	#print "sleeping for 10 seconds to let file system catch up.\n" if $verbose;
	#sleep 10;
	print "starting samtools sort of $file_in " . localtime() . "\n" if $verbose;
	print "executing samtools sort $file_out_root.bam $file_out_root.sorted";
	system "samtools sort $file_out_root.bam $file_out_root.sorted";
	print "executing samtools index $file_out_root.sorted.bam $file_out_root.sorted.bai " . localtime() . "\n" if $verbose;
	system "samtools index $file_out_root.sorted.bam $file_out_root.sorted.bai";
	print "executing samtools idxstats $file_out_root.sorted.bam > $file_out_root.counts " . localtime() . "\n" if $verbose;
	system "samtools idxstats $file_out_root.sorted.bam > $file_out_root.counts";
	print "finished $file_in at " . localtime() . "\n\n" if $verbose;
}

exit;
