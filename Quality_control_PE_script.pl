#!/usr/bin/env perl
#Quality_control_PE_script.pl 
#Upendra Kumar Devisetty
# Use this script to perform QC on PE samples
use warnings;
use strict;
use File::Basename;


if (@ARGV==0) {
        print "first argument is directory path for fastq file\n";
        print "second argument is directory path to barcode file\n";
        exit;
}

my @files1 = `ls $ARGV[0]/*R1*.gz`;
my @files2 = `ls $ARGV[0]/*R2*.gz`;


die "no fastq files found in $ARGV[0] --" if (@files1 && @files2 == 0 );

my $file_counter = 0;
        foreach  my $f1 (@files1) {
        my $f2 = $files2[$file_counter];
        chomp $f1;
        chomp $f2;
        my($filename1, $directories1, $suffix1) = fileparse($f1, "_s_1.fq");
        my($filename2, $directories2, $suffix2) = fileparse($f2, "_s_2.fq");
        print "FILENAME - $filename1, DIR -  $directories1, SUFFIX -  $suffix1\n";
        print "FILENAME - $filename2, DIR -  $directories2, SUFFIX -  $suffix2\n";
        system "mkdir $f1.fastqc_result";               
        system "mkdir $f2.fastqc_result";
        system "fastqc -o $f1.fastqc_result $f1";
        system "fastqc -o $f2.fastqc_result $f2";
        system "gunzip -c $f1 | perl Illumina_fastq_filter-JP.pl - > $f1.filter.fastq";                 
        system "gunzip -c $f2 | perl Illumina_fastq_filter-JP.pl - > $f2.filter.fastq";
        system "./fastq_quality_filter -Q 33 -q 30 -p 95 -i $f1.filter.fastq -o $f1.filter.fastq.qf95.fastq";
        system "./fastq_quality_filter -Q 33 -q 30 -p 95 -i $f2.filter.fastq -o $f2.filter.fastq.qf95.fastq";
        system "mkdir $f1.filter.fastq.qf95.fastq.fastqc_result";
        system "mkdir $f2.filter.fastq.qf95.fastq.fastqc_result";
        system "fastqc -o $f1.filter.fastq.qf95.fastq.fastqc_result $f1.filter.fastq.qf95.fastq";                
        system "fastqc -o $f2.filter.fastq.qf95.fastq.fastqc_result $f2.filter.fastq.qf95.fastq";
        system "perl adapter_counter_remover.pl $f1.filter.fastq.qf95.fastq";
        system "perl adapter_counter_remover.pl $f2.filter.fastq.qf95.fastq";
        system "mv $f1.filter.fastq.qf95.fastq.no_adapters $f1.filter.fastq.qf95.fastq.no_adapters.fastq";
        system "mv $f2.filter.fastq.qf95.fastq.no_adapters $f2.filter.fastq.qf95.fastq.no_adapters.fastq";
        system "perl barcode_split_trim.pl --barcode $ARGV[1] --list $f1.filter.fastq.qf95.fastq.no_adapters.fastq --suffix 1";
        system "perl barcode_split_trim.pl --barcode $ARGV[1] --list $f2.filter.fastq.qf95.fastq.no_adapters.fastq --suffix 2";
}
