Tutorial - How to run Quality Control and mapping pipeline on single end reads generated from Hiseq 2000 and v1.8 on iPlant

Updated: 09/21/13

By Upendra Kumar Devisetty ([upendrakumar.devisetty@googlemail.com](upendrakumar.devisetty@googlemail.com))

* Requirements


    - This tutorial is prepared and tested on atmosphere. Atmosphere is a cloud service that allows users to launch their own isolated virtual working environment and software. It should be easy to adapt for other systems. 
    - This first part of the pipleline i.e downloading the raw files is mainly written for QB3 sequencing facility service. You may or may not use this part if you are downloading the sequences from other servicers.
    - The following assumes that you are working in your own copy of the tutorial directory.
    - I found that one of the cpan module is missing while running one of the perl script. So in case you get this error `Can't locate Text/Table.pm in @INC (@INC contains: /usr/local/src/vcftools_0.1.10/perl /usr/local/src/tabix-0.2.6/perl /etc/perl /usr/local/lib/perl/5.14.2 /usr/local/share/perl/5.14.2 /usr/lib/perl5 /usr/share/perl5 /usr/lib/perl/5.14 /usr/share/perl/5.14 /usr/local/lib/site_perl .) at barcode_split_trim.pl line 23` just run this command `cpanm --sudo Text/Table.pm` and it should load the module.

1. Make a directory and download the raw sequence data from the sequencing centre

        $ ./download_concatenate.sh

        - This script will first create a directory onto which the raw sequence data       
          files will be downloaded directly from the sequencing centre and moves the       
          files into current directory. 
        - Then it removes the sequencing directory. Just replace the `user name`,     
          `password` and the `target directory` 
        - It then concatenates all the gipped files to one big gipped file.

2. Run the Quality control script

        $ ./Quality_control_SE.pl --gz tutorial --file setE_plate3_barcodes.txt --out    
          tutorial 

        - This script will perform everything from fastqc analysis to barcode   
          splitting. 
        - It takes the concatenated file from step (1) and run the fastqc on    
          it first. Then the concatenated file will be unzipped on the fly and then  
          runs the Illumina quality filter first to remove all the reads which have "y"     
          in their read headers.
        - Then it uses `fastx_quality_filter` to remove all the reads except those have 
          `q=20; p=85`. Again it runs the fastqc on the filtered reads so that you can      
          compare the quality filtering before and after quality filtering. 
        - Later it runs to remove all reads that have adapters in them and finally it   
          splits the reads according to barcode which is provided in a separate file. 
          An example of barcode file is given in a sample_file folder. 
        - Once the barcode splitting is done, i recommend to run the Mike's   
          [auto_barcode](https://github.com/mfcovington/auto_barcode/ "auto_barcode")    
          tool. 
        
3.  Run the mapping script

    (1) Index the genome/cDNA to be used as reference 

        $ ./bwa index -p ref.fasta ref.fasta 

    (2) Run the mapping script

        $ ./runBWA.pl --fq tutorial --index ref.fasta --out tutorial 

        - This script will take the "fq" files produced at the end of step (2) and will   
        map the reads using bwa against indexed reference.