#!/usr/bin/env perl
use warnings;
use strict;
use File::Basename;
use Getopt::Long;
use Cwd;
#Quality_control_SE_script.pl
#./Quality_control_SE.pl --gz tutorial --file ../kazu_palmer_libraries/setE_plate3_barcodes.txt --out tutorial &

my ($gz, $barcode, $verbose, $help, );
my $out_dir = getcwd;

my $options = GetOptions (
        "gz=s"          =>      \$gz,
        "file=s"        =>      \$barcode,
        "out=s"         =>      \$out_dir,
        "verbose"       =>      \$verbose,
        "help"          =>      \$help,
);

my $usage = "
        USAGE: Quality_control_SE.pl
                --gz            </path/to/*.gz directory>
                --file          </path/to/barcode file directory>
                --out           </path/to/output/directory/> (default= ./ )
                --verbose       **verbose output**
                --help          **print this help**

Function Summary:
Quality_control_SE.pl processes gipped fastq files with fastqc, fastx tool kit, adapter remover and finally splits the barcode.
";

die $usage if $help;
die $usage unless defined ($gz && $barcode);

my @files = `ls $gz/*all.fastq.gz`;
chomp @files;
#print "@files\n";
 
foreach my $f(@files) {
      chomp $f;
       my ( $filename, $directories, $suffix ) = fileparse( $f, ".gz" );
       print "FILENAME - $filename, DIR -  $directories, SUFFIX -  $suffix\n";
       system "mkdir $f.fastqc_result";
       system "fastqc -o $f.fastqc_result $f";  
       system "gunzip -c $f | perl Illumina_fastq_filter-JP.pl - > $f.filter.fastq";
       system "./fastq_quality_filter -Q 33 -q 20 -p 95 -i $f.filter.fastq -o $f.filter.fastq.qf95.fastq";
       system "mkdir $f.filter.fastq.qf95.fastq.fastqc_result";
       system "fastqc -o $f.filter.fastq.qf95.fastq.fastqc_result $f.filter.fastq.qf95.fastq";
       system "perl adapter_counter_remover.pl $f.filter.fastq.qf95.fastq";
       system "mv $f.filter.fastq.qf95.fastq.no_adapters $f.filter.fastq.qf95.fastq.no_adapters.fastq";  
       system "perl barcode_split_trim.pl --barcode $barcode --list $f.filter.fastq.qf95.fastq.no_adapters.fastq";
}