#!/bin/bash

mkdir tutorial &&
cd tutorial &&

echo "\n\n### Getting raw sequence files from QB3 ###"

wget -r 'ftp://GSLUSER:B206Stanley@islay.qb3.berkeley.edu///VCGSL_FTP/130903_HS2B/Project_Maloof_J/Sample_JKNE3' &&

mv islay.qb3.berkeley.edu/VCGSL_FTP/130903_HS2B/Project_Maloof_J/Sample_JKNE3/* .

rm -r islay.qb3.berkeley.edu

cat *.gz > JKNE3_NoIndex_L003_R1_all.fastq.gz &&

echo "\nDone!"

#EOF