#!/usr/bin/perl
# adapter_counter.pl
# Mike Covington
# created: 2011-10-07
#
# Description: Counts the number of reads and removes in fastq files that contain an adapter sequence and 
#
use strict; use warnings;

die "USAGE: adapter_counter.pl <fastq_file(s)>\n" unless (@ARGV >= 1);
my @fastq_files = @ARGV;

my $fwd_adapter = "GATCGGAAGAGCGGTTCAGCAGGAATGCCGAG";
my $rev_adapter = "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT";

print "\nADAPTER COUNT SUMMARY:\n";

foreach my $file_name (@fastq_files) {
	open (FILE_IN, $file_name);
	my $out_file_name = "$file_name.no_adapters";
	open (FILE_OUT, ">$out_file_name");
	my $adapter_count = 0;
	my $read_count = 0;
	while (my $seq_head_line = <FILE_IN>) {
		my $seq_line = <FILE_IN>;
		my $qual_head_line = <FILE_IN>;
		my $qual_line = <FILE_IN>;
		if ($seq_line =~ m/$fwd_adapter|$rev_adapter/) {
			$adapter_count++;
		}else{
			print FILE_OUT $seq_head_line . $seq_line . $qual_head_line . $qual_line;
		}
		$read_count++;
		print "Progress: $adapter_count adapters in $read_count reads...\r" if $read_count%100000==0; # Progress meter
	} # while
	print "                                                                \r"; # Progress meter
	print "\t$file_name: \t$adapter_count out of $read_count reads\n";
	print "\t\t(Writing w/o adapters to $out_file_name)\n";
	close (FILE_IN);
	close (FILE_OUT);
} # foreach

exit;
