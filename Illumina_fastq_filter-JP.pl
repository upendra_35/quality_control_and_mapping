#!/usr/bin/ebv perl
use warnings;
# usage: perl Illumina_fastq_filter-JP.pl filename > outname
 
my $n = 0; #line counter
open (FILENAME, "$ARGV[0]") or die "Can't find $ARGV[0]\n";
while (<FILENAME>) {
        chomp;
        if ($_ =~ /^@\S+\s\d\:N/) {
                $n = 4;
        }
        if ($n > 0) {
                print "$_\n";
                $n--;
        }
}
close FILENAME;